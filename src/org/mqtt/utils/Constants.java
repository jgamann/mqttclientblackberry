package org.mqtt.utils;


public class Constants {
	
	//Device Data
	public static final String VERSION 							= "v1.0.0";
	
	
	//GPS Constants
	public static final int TEN_MINUTES_IN_MILISECONDS 			= 600000;
	public static final int HALF_MINUTE_SECONDS_IN_MILISECONDS 	= 30000;
	public static final double GPS_REPORT_DISTANCE 				= 150;
	public static double latitude 								= 43.295326;
	public static double longitude 								= -2.864227;
	public static float altitude;
	public static float speed;
	public static float course;
	
	public static String provider 								= "gps";
	public static float accuracy;
	public final static String GPS_PROVIDER 					= "gps";
	public final static double EARTH_RADIUS_KM 					= 6371;

	//Storage persistent
	public static final long PERSISTENT_STORE_ID 				= 0x220d57d6848faeffL;
	public static final long PERSISTENT_PENDING_MSG				= 0xbf768b0f3ae726daL;
	public static String LicenceStored;
	
	//General Constants
	public static final boolean NO_BEEP 						= false;
	public static final boolean YES_BEEP 						= true;
	public static final boolean STORE_IF_FAILS 					= true;
	public static final boolean NOT_STORE_IF_FAILS 				= false;
	
	//MQTT
	public static final String PREFIX_MQTT_BROKER				= "tcp://";
	

}
