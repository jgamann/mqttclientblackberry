package org.mqtt.utils;

import java.util.Vector;

import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.WLANInfo;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;


public final class Utils {
	
		
	public static void playBeep(int duration, byte tone, int volume){
		try {
//			byte C4 = ToneControl.C4;; 
//			byte J4 = (byte)(C4 + 30);
		    Manager.playTone(tone, duration /* ms */, volume );
		} catch (MediaException e) { }
	}
	
	public static String getConnectionString(String connectionString){
	    if(WLANInfo.getWLANState()==WLANInfo.WLAN_STATE_CONNECTED){
	    	connectionString=connectionString+";interface=wifi";
	    }else{
	    	return connectionString;
	    	//connectionString=connectionString+";deviceside=true;ConnectionTimeout=360;apn=movistar.es;tunnelauthusername=MOVISTAR;tunnelauthpassword=MOVISTAR";
	    }
	    return connectionString;
	}
	
	public static boolean persistirData(long key, Object value){
		//PersistentStore.getPersistentObject
		try{
			PersistentObject persist = PersistentStore.getPersistentObject(key);
			persist.setContents(value);
			persist.commit();
			return true;
		}catch(Exception e){
			System.err.println(e);
			return false;
		}
	}

	
	public static Object obtenerData(long key){
		//PersistentStore.getPersistentObject
		try{
			PersistentObject persist = PersistentStore.getPersistentObject(key);
			return persist.getContents();
		}catch(Exception e){
			System.err.println(e);
			return null;
		}
	}

	public static void setImageBackground(MainScreen screen) {
		try{
        	Bitmap bitmap = Bitmap.getBitmapResource( "img/bg_640.png" );
        	Background bg = BackgroundFactory.createBitmapBackground(bitmap,Background.POSITION_X_LEFT, 
                    Background.POSITION_Y_TOP, 
                    Background.REPEAT_NONE);
        	screen.getMainManager().setBackground(bg);
        }catch(Exception e){
        	System.err.println(e);
        }
		
	}

	public static void setColorBackground(MainScreen screen) {
		try{
        	Background bg = BackgroundFactory.createSolidBackground(Color.DARKSLATEGRAY);
        	screen.getMainManager().setBackground(bg);
        }catch(Exception e){
        	System.err.println(e);
        }
		
	}
	
	public static float getGreater(float horizontalAccuracy, float verticalAccuracy) {
		if(horizontalAccuracy>verticalAccuracy)return horizontalAccuracy;
		return verticalAccuracy;
	}

	public static void clearPendingMsg() {
		try{
			PersistentStore.destroyPersistentObject(Constants.PERSISTENT_PENDING_MSG);
		}catch (Exception e) {
		}
	}

	public static String testPendingMsg(String msg) {
		PersistentObject persist = PersistentStore.getPersistentObject(Constants.PERSISTENT_PENDING_MSG);
		StringBuffer response = new StringBuffer();
		if(persist.getContents()!=null){
			Vector v = (Vector)persist.getContents();
			for(int i=0;i<v.size();i++){
				response.append(v.elementAt(i));
			}
		}
		response.append(msg);
		return response.toString();
	}

	public static void addPendingMsg(String msg) {
		try{
			PersistentObject persist = PersistentStore.getPersistentObject(Constants.PERSISTENT_PENDING_MSG);
			Vector v = new Vector();
			if(persist.getContents()!=null){
				v = (Vector)persist.getContents();
			}
			v.addElement(msg);
			persistirData(Constants.PERSISTENT_PENDING_MSG,v);
		}catch(Exception e){
			System.err.println(e);
		}
	}
}
