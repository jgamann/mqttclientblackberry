package org.mqtt;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.mqtt.utils.Constants;
import org.mqtt.utils.Utils;

/**
 * A class extending the MainScreen class, which provides default standard
 * behavior for BlackBerry GUI applications.
 */
public final class MyMQTTScreen extends MainScreen
{
	private LabelField _labelHost;
	private EditField _host;
	private LabelField _labelPort;
	private EditField _port;
	private LabelField _labelUser;
	private EditField _user;
	private LabelField _labelPass;
	private EditField _pass;
	MQTTHandler mqttH; 
	public static int DEF_QOS = 1;
    /**
     * Creates a new MyMQTTScreen object
     */
    public MyMQTTScreen()
    {
    	super(MainScreen.VERTICAL_SCROLL | MainScreen.VERTICAL_SCROLLBAR);
    	Utils.setColorBackground(this);
        // Set the displayed title of the screen       
        setTitle("MQTT Client");
        
        HorizontalFieldManager _hfmHost = new HorizontalFieldManager(Field.FIELD_VCENTER);
        
        _labelHost = new LabelField("IP Host: ") {
			protected void applyTheme(Graphics g, boolean arg1) {
				g.setColor(Color.WHITE);
				super.applyTheme(g, arg1);
			}
		};
		_labelHost.setFont(_labelHost.getFont().derive(Font.BOLD));
		_host = new EditField(null, null, 15, EditField.FILTER_PHONE
				| EditField.FIELD_VCENTER | EditField.FIELD_HCENTER) {
			protected void applyTheme(Graphics g, boolean arg1) {
				g.setColor(Color.AQUAMARINE);
				super.applyTheme(g, arg1);
			}
		};
		
		_hfmHost.add(_labelHost);
		_hfmHost.add(_host);

		add(_hfmHost);
		
		HorizontalFieldManager _hfmPort = new HorizontalFieldManager(Field.FIELD_VCENTER);
		
		_labelPort = new LabelField("Port: ") {
			protected void applyTheme(Graphics g, boolean arg1) {
				g.setColor(Color.WHITE);
				super.applyTheme(g, arg1);
			}
		};
		_labelPort.setFont(_labelPort.getFont().derive(Font.BOLD));
		_port = new EditField(null, null, 4, EditField.FILTER_NUMERIC
				| EditField.FIELD_VCENTER | EditField.FIELD_HCENTER) {
			protected void applyTheme(Graphics g, boolean arg1) {
				g.setColor(Color.AQUAMARINE);
				super.applyTheme(g, arg1);
			}
		};
		
		_hfmPort.add(_labelPort);
		_hfmPort.add(_port);

		add(_hfmPort);
		
		
		HorizontalFieldManager _hfmUser = new HorizontalFieldManager(Field.FIELD_VCENTER);
		
		_labelUser = new LabelField("Usuario: ") {
			protected void applyTheme(Graphics g, boolean arg1) {
				g.setColor(Color.WHITE);
				super.applyTheme(g, arg1);
			}
		};
		_labelUser.setFont(_labelPort.getFont().derive(Font.BOLD));
		_user = new EditField(null, null, 20, EditField.FILTER_DEFAULT
				| EditField.FIELD_VCENTER | EditField.FIELD_HCENTER) {
			protected void applyTheme(Graphics g, boolean arg1) {
				g.setColor(Color.AQUAMARINE);
				super.applyTheme(g, arg1);
			}
		};
		
		_hfmUser.add(_labelUser);
		_hfmUser.add(_user);

		add(_hfmUser);
		
		HorizontalFieldManager _hfmPass = new HorizontalFieldManager(Field.FIELD_VCENTER);
		
		_labelPass = new LabelField("Password: ") {
			protected void applyTheme(Graphics g, boolean arg1) {
				g.setColor(Color.WHITE);
				super.applyTheme(g, arg1);
			}
		};
		_labelPass.setFont(_labelPort.getFont().derive(Font.BOLD));
		_pass = new EditField(null, null, 20, EditField.FILTER_DEFAULT
				| EditField.FIELD_VCENTER | EditField.FIELD_HCENTER) {
			protected void applyTheme(Graphics g, boolean arg1) {
				g.setColor(Color.AQUAMARINE);
				super.applyTheme(g, arg1);
			}
		};
		
		_hfmPass.add(_labelPass);
		_hfmPass.add(_pass);

		add(_hfmPass);

		ButtonField buttonField_1 = new ButtonField("Connect",
				ButtonField.CONSUME_CLICK | ButtonField.FIELD_HCENTER
						| ButtonField.VCENTER);
		add(buttonField_1);
		buttonField_1.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field arg0, int arg1) {
				Application.getApplication().invokeLater(new Runnable() {
					public void run() {
						try {
							System.out.println("connecting");
							// ########## MQTT handling ##############
							mqttH = new MQTTHandler("BlackBerry", Constants.PREFIX_MQTT_BROKER + _host.getText() + ":" + _port.getText(), _user.getText(), _pass.getText());

							// connect to MQTT Brocker
							System.out.println("Connecting to MQTT brocker...");
							try {
								mqttH.connectToBrocker();
								mqttH.subscribe("#", DEF_QOS);
							} catch (MqttSecurityException e1) {
								e1.printStackTrace();
							} catch (MqttException e1) {
								e1.printStackTrace();
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							System.out.println(e);
						}
					}
				});
			}
		});
    }
}
